//
//  InfoViewController.h
//  Oxidizing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 11/14/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface InfoViewController : BaseViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *infoView;
@end
