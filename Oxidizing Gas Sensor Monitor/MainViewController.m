//
//  MainViewController.m
//  Oxidizing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 11/14/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize counter;
@synthesize powerBtn;
@synthesize timer;
@synthesize dataDisplay;
@synthesize baselineArray;
@synthesize baseline;
@synthesize selectedSensitivity;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    baselineArray = [[NSMutableArray alloc] init];
    
    // Start in low sensitivity mode
    [self setSensitivity:0];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sensitivitySelected:(id)sender {
    if (sender == self.sensBtn_High) {
        [self setSensitivity:2];
    }
    else if (sender == self.sensBtn_Med) {
        [self setSensitivity:1];
    }
    else {
        [self setSensitivity:0];
    }
}

- (IBAction)baselineReset:(id)sender {

    counter = 10;
    
}


- (IBAction)powerToggle:(id)sender {
    // Don't do anything if we're not connected
    if (![self.delegate.myDrone isDroneConnected]) {
        [[[UIAlertView alloc] initWithTitle:@"Not Connected" message:@"You are not currently connected to your Sensordrone. Please connect first from the Connections page." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    // We should be connected if we've made it this far
    // Were we toggled on or off?
    BOOL status = [self.delegate.myDrone oxidizingGasStatus];
    if (status) {
        // Turn it off we're on
        [self.delegate.myDrone disableOxidizingGas];
        [self setLEDs:0]; // Shut off any LEDs
    }
    else {
        // Turn it on if we're off
        [self.delegate.myDrone enableOxidizingGas];

    }
}



-(void)doOnOxidizingGasEnabled {
    //NSLog(@"Oxidizing sensor has been enabled...");
    // Reset our counter
    counter = 15;
    // Start measuring every 1 Second
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self.delegate.myDrone selector:@selector(measureOxidizingGas) userInfo:nil repeats:YES];
    
}

-(void)doOnOxidizingGasDisabled {
    //NSLog(@"Oxidizing sensor has been disabled...");

    // Disable our timer!
    [timer invalidate];
    timer = nil;
    // Tell the user we've shut off
    [self.dataDisplay setText:@"Sensor Off"];
    
}

-(void)setLEDs:(int)nLit {
    // We want our LEDs to be on from left to right
    
    // LED 1
    if (nLit > 0) {
        [self.led_1 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_1 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 2
    if (nLit > 1) {
        [self.led_2 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_2 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 3
    if (nLit > 2) {
        [self.led_3 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_3 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 4
    if (nLit > 3) {
        [self.led_4 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_4 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 5
    if (nLit > 4) {
        [self.led_5 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_5 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 6
    if (nLit > 5) {
        [self.led_6 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_6 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 7
    if (nLit > 6) {
        [self.led_7 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_7 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 8
    if (nLit > 7) {
        [self.led_8 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_8 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
    // LED 9
    if (nLit > 8) {
        [self.led_9 setImage:[UIImage imageNamed:@"led_on.png"]];
    }
    else {
        [self.led_9 setImage:[UIImage imageNamed:@"led_off.png"]];
    }
}


-(void)playGeigerTicks:(int)nTicks {
    NSString *fileName;
    if (nTicks == 9) {
        fileName = @"G9";
    }
    else if (nTicks == 8) {
        fileName = @"G8";
    }
    else if (nTicks == 7) {
        fileName = @"G7";
    }
    else if (nTicks == 6) {
        fileName = @"G6";
    }
    else if (nTicks == 5) {
        fileName = @"G5";
    }
    else if (nTicks == 4) {
        fileName = @"G4";
    }
    else if (nTicks == 3) {
        fileName = @"G3";
    }
    else if (nTicks == 2) {
        fileName = @"G2";
    }
    else if (nTicks == 1){
        fileName = @"G1";
    }
    else {
        fileName = @"NOSOUND";
    }
    
    if (![fileName  isEqual: @"NOSOUND"]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"m4a"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
        
        self.geiger = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
        [self.geiger setVolume:1];
        [self.geiger setDelegate:self];
        [self.geiger prepareToPlay];
        [self.geiger play];
    }
    
}

-(void)setSensitivity:(int)level {
    selectedSensitivity = level;
    if (level == 2) {
        [self.sensBtn_High setImage:[UIImage imageNamed:@"high_button_on.png"] forState:UIControlStateNormal];
        [self.sensBtn_Med setImage:[UIImage imageNamed:@"med_button.png"] forState:UIControlStateNormal];
        [self.sensBtn_Low setImage:[UIImage imageNamed:@"low_button.png"] forState:UIControlStateNormal];
    }
    else if (level == 1) {
        [self.sensBtn_High setImage:[UIImage imageNamed:@"high_button.png"] forState:UIControlStateNormal];
        [self.sensBtn_Med setImage:[UIImage imageNamed:@"med_button_on.png"] forState:UIControlStateNormal];
        [self.sensBtn_Low setImage:[UIImage imageNamed:@"low_button.png"] forState:UIControlStateNormal];

    }
    else {
        [self.sensBtn_High setImage:[UIImage imageNamed:@"high_button.png"] forState:UIControlStateNormal];
        [self.sensBtn_Med setImage:[UIImage imageNamed:@"med_button.png"] forState:UIControlStateNormal];
        [self.sensBtn_Low setImage:[UIImage imageNamed:@"low_button_on.png"] forState:UIControlStateNormal];

    }
        
}
-(void) doOnOxidizingGasMeasured {

    // To re-set the baseline, all we'll need to do is change counter!
    if (counter < 0) {
        [self.dataDisplay setText:[NSString stringWithFormat:@"%.0f Ohm", self.delegate.myDrone.measuredOxidizingGasInOhm]];
        // Set a scale
        float val = self.delegate.myDrone.measuredOxidizingGasInOhm / baseline;
        //NSLog(@"Measurement / Baseline ratio: %0.0f Ohm", val);
        if (val < threshold_0[selectedSensitivity]) {
            [self setLEDs:0];
        }
        else if (val < threshold_1[selectedSensitivity]) {
            [self setLEDs:1];
            [self playGeigerTicks:1];
        }
        else if (val < threshold_2[selectedSensitivity]) {
            [self setLEDs:2];
            [self playGeigerTicks:2];
        }
        else if (val < threshold_3[selectedSensitivity]) {
            [self setLEDs:3];
            [self playGeigerTicks:3];
        }
        else if (val < threshold_4[selectedSensitivity]) {
            [self setLEDs:4];
            [self playGeigerTicks:4];
        }
        else if (val < threshold_5[selectedSensitivity]) {
            [self setLEDs:5];
            [self playGeigerTicks:5];
        }
        else if (val < threshold_6[selectedSensitivity]) {
            [self setLEDs:6];
            [self playGeigerTicks:6];
        }
        else if (val < threshold_7[selectedSensitivity]) {
            [self setLEDs:7];
            [self playGeigerTicks:7];
        }
        else if (val < threshold_8[selectedSensitivity]) {
            [self setLEDs:8];
            [self playGeigerTicks:8];
        }
        else {
            [self setLEDs:9];
            [self playGeigerTicks:9];
        }
        
    }
    else if (counter <=15) {
        if (counter <= 9) {
            [baselineArray addObject:[[NSNumber alloc] initWithFloat:self.delegate.myDrone.measuredOxidizingGasInOhm]];
        }
        // If we made it to zero, set the baseline property
        if (counter == 0) {
            [self calculateBaseline];
        }
        [self.dataDisplay setText:[NSString stringWithFormat:@"Setting Baseline: %d", counter]];
        [self setLEDs:counter];
        counter--;
    }
    
}

-(void)calculateBaseline {
    float sum = 0;
    for (int i = 0; i < [baselineArray count]; i++) {
        sum += [[baselineArray objectAtIndex:i] floatValue];
    }
    // Calcualte the average
    [self setBaseline:sum/[baselineArray count]];
    //NSLog(@"Basline set: %0.0f Ohm", baseline);
    // Clear our array for next time
    [baselineArray removeAllObjects];
}
@end
